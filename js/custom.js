
$(document).ready(function(){

  
  $("#resp-toggle-btn").on('click', function(){
  	$("body").toggleClass('navTrue');
  })


  $(".faq-list").on('click', 'dt', function() {
  	$(this).toggleClass('active').next().slideToggle();
  })

  if ($(".catalog__item__imgs").length){
  	$(".catalog__item__imgs").each(function(){
  		$(this).owlCarousel({
  			singleItem : true,
  			dataItems : 1,
  			pagination : false,
  			navigation : true,
  			navigationText : ["", ""]
  		})
  	})
  }


})